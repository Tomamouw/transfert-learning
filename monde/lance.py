from nes_py.wrappers import JoypadSpace
import gym_super_mario_bros
from gym_super_mario_bros.actions import SIMPLE_MOVEMENT

import os
monde = 2
niv = 4

env = gym_super_mario_bros.make(f'SuperMarioBros-{monde}-{niv}-v0')
env = JoypadSpace(env, SIMPLE_MOVEMENT)

done = True
for step in range(800):
    i = step
    if done:
        state = env.reset()
    state, reward, done, info = env.step(env.action_space.sample())
    env.render()
    if step%40 == 0 :
        
        
        os.system(f"gnome-screenshot --window --file=screenadd2{monde}{niv}{i}.png")
    

env.close()
